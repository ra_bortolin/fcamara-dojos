﻿using NUnit.Framework;

namespace FCamara.Dojo1
{
    [TestFixture]
    class Testes
    {
        public static int[] Numeros { get { return new[] { 6, 9, 15, -2, 92, 11 }; } }
        public static int[] Numeros2 { get { return new[] { 5, 1, 37, -8, 52,-3, 99 }; } }

        public static int[] Numeros3 { get { return new[] { 5, 1, 37, 8, 52, 3, 99 }; } }

        public static int[] Numeros4 { get { return new[] { -5, -1, -37, -8, -52, -3, -99 }; } }

        [Test]
        public void Valor_Minimo_Deve_Ser_Menos_2()
        {
            var valorMinimo = Program.CalculaValorMinimo(Numeros);

            Assert.AreEqual(-2, valorMinimo);
        }

        [Test]
        public void Valor_Minimo_Deve_Ser_Maximo_92()
        {
            var valorMaximo = Program.CalculaValorMaximo(Numeros);

            Assert.AreEqual(92, valorMaximo);
        }

        [Test]
        public void Quantidade_De_Numeros_Vetor_Eh_Igual_6()
        {
            var quantidadeNumerosVetor = Program.CalcularQuantidadeNumeroVetor(Numeros);

            Assert.AreEqual(6, quantidadeNumerosVetor);
        }

        [Test]
        public void Valor_Medio_Igual_2183()
        {
            var valorMedio = Program.CalcularValorMedio(Numeros);

            Assert.AreEqual(21.83, valorMedio);
        }
        [Test]
        public void Valor_Medio_Igual_26_14()
        {
            var valorMedio = Program.CalcularValorMedio(Numeros2);

            Assert.AreEqual(26.14, valorMedio);
        }

        [Test]
        public void Valor_Minimo_Deve_Ser_Menos_8()
        {
            var valorMinimo = Program.CalculaValorMinimo(Numeros2);

            Assert.AreEqual(-8, valorMinimo);
        }

        [Test]
        public void Valor_Minimo_Deve_Ser_Maximo_99()
        {
            var valorMaximo = Program.CalculaValorMaximo(Numeros2);

            Assert.AreEqual(99, valorMaximo);
        }
        [Test]
        public void Quantidade_De_Numeros_Vetor_Eh_Igual_7()
        {
            var quantidadeNumerosVetor = Program.CalcularQuantidadeNumeroVetor(Numeros2);

            Assert.AreEqual(7, quantidadeNumerosVetor);
        }

        [Test]
        public void Passando_Vetor_3_Valor_Minimo_Deve_Ser_1()
        {
            var valorMinimo = Program.CalculaValorMinimo(Numeros3);

            Assert.AreEqual(1, valorMinimo);
        }

        [Test]
        public void Valor_Maximo_Deve_Ser_Menos_1()
        {
            var valorMaximo = Program.CalculaValorMaximo(Numeros4);

            Assert.AreEqual(-1, valorMaximo);
        }

    }
}
