﻿



using System;
using System.Linq;

namespace FCamara.Dojo1
{
    public class Program
    {

        public static int CalculaValorMinimo(int[] array)
        {         
            return array.Min();
        }

        public static int CalculaValorMaximo(int[] array)
        {
            return array.Max();
        }

        public static int CalcularQuantidadeNumeroVetor(int[] array)
        {
            return array.Count();
            
        }
        public static double CalcularValorMedio(int[] array)
        {
            return Math.Round(array.Average(), 2, MidpointRounding.AwayFromZero);
            
        }
      
    }
}
