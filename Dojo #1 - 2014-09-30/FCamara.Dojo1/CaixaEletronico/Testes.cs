﻿using NUnit.Framework;
using System.Linq;

namespace FCamara.Dojo1.CaixaEletronico
{
    [TestFixture]
    class Testes
    {



        [Test]
        public void Sacar_Trinta_Reais_Caixa_Dar_Um_Nota_20_E_10()
        {
            var valorASacar = 30;
            var notas = CaixaEletronico.Sacar(valorASacar);

            Assert.IsTrue(notas.Contains(10));
            Assert.IsTrue(notas.Contains(20));

            Assert.AreEqual(valorASacar, notas.Sum());
        }

        [Test]
        public void Sacar_Oitenta_Reais_Caixa_Dar_Um_Nota_20_E_10_E_50()
        {
            var valorASacar = 80;
            var notas = CaixaEletronico.Sacar(valorASacar);

            Assert.IsTrue(notas.Contains(10));
            Assert.IsTrue(notas.Contains(20));
            Assert.IsTrue(notas.Contains(50));

            Assert.AreEqual(valorASacar, notas.Sum());
        }

        [Test]
        public void Sacar_90_Reais_Caixa_Dar_2_Notas_20_E_50()
        {
            var valorASacar = 90;
            var notas = CaixaEletronico.Sacar(valorASacar);

            Assert.IsTrue(notas.Contains(20));
            Assert.IsTrue(notas.Contains(50));

            Assert.AreEqual(valorASacar, notas.Sum());
        }


        [Test]
        public void Sacar_140_Reais_Caixa_Dar_1_Nota_100_20_20()
        {
            var valorASacar = 140;
            var notas = CaixaEletronico.Sacar(valorASacar);


            Assert.IsTrue(notas.Contains(20));
            Assert.IsTrue(notas.Count(x => x == 20) == 2);
            Assert.IsTrue(notas.Contains(100));

            Assert.AreEqual(valorASacar, notas.Sum());
        }

        [Test]
        [ExpectedException]
        public void Sacar_15_Reais_Valor_Invalido()
        {
            var valorASacar = 15;
            var notas = CaixaEletronico.Sacar(valorASacar);

            Assert.Fail();
        }

        [Test]
        public void Sacar_10()
        {
            var valorASacar = 10;
            var notas = CaixaEletronico.Sacar(valorASacar);


            Assert.IsTrue(notas.Contains(10));
            

            Assert.AreEqual(valorASacar, notas.Sum());
        }

    }
}
