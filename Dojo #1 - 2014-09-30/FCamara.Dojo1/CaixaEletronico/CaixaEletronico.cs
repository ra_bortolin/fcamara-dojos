﻿





using System;
using System.Collections.Generic;
using System.Linq;

namespace FCamara.Dojo1.CaixaEletronico
{
    public class CaixaEletronico
    {
        public static decimal[] Notas { get { return new[] { 10m, 20m, 50m, 100m }; } }
        public static decimal[] Sacar(decimal valorSaque)
        {
            var notasSacar = new List<decimal>();

            foreach (var nota in Notas.OrderByDescending(n => n))
            {
                while (valorSaque >= nota)
                {

                    notasSacar.Add(nota);
                    valorSaque -= nota;

                }
            }

            if (valorSaque != 0)
                throw new Exception("Valor inválido.");

            return notasSacar.ToArray();
        }

    }
}
